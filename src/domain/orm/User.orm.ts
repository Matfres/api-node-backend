import { userEntity } from "../entities/User.entity";

import { LogError, LogSuccess } from "../../utils/logger";

//* CRUD

/**
 *  * Method obtain all Users from Collection "User" in Mongo Server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {
    try {
        let userModel = userEntity();
        // * Search all user
        return await userModel.find({ isDelete: false })
    } catch (error) {
        LogError(`[ORM ERROR]: Getting all Users: ${error} `)
    }
}