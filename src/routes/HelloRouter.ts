import { BasicResponse } from "../controller/types";
import express, { Request, Response  } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";

// * Router from express

let helloRouter = express.Router();

// * GET -> http://localhost:3000/api/hello/
helloRouter.route('/').get(async (req: Request, res:Response) => {
    // obtain Query param
    let name: any = req?.query?.name;
    LogInfo(`Query Param: ${name}`)
    // Controller Instance to excute method
    const controller: HelloController = new HelloController();
    // Obtian Response
    const response: BasicResponse = await controller.getMessage(name)
    // Send to client 
    return res.send(response);
});

export default helloRouter;