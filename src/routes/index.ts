/**
 * * Root Router
 * * Redirection to Routers
*/
import express, { Request, Response, Express } from "express";
import helloRouter from "./HelloRouter";
import { LogInfo } from "../utils/logger";

// * Server Instace 
let server = express();

// * Router instance
let rootRouter = express.Router();

// * Activate for requests to http://localhost:3000/api
// * GET: http://localhost:3000/api/
rootRouter.get('/', (_req: Request, res: Response) => {
    // send hello world
    LogInfo(`GET: http://localhost:3000/api/`);
    res.send('hello world + backend node app')
});

// * Redirections to Routers and Controllers
server.use('/', rootRouter);
server.use('/hello', helloRouter);

export default server;