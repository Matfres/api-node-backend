import express, { Express, Request, Response } from "express";
import cors from 'cors';
import helmet from "helmet";
// TODO: HTTPS
import rootRouter from '../routes';

// * Create Express APP
const server: Express = express();

// * Define SERVER to use "/api" and use rootRouter
// * From this point onover: localhost:3000/api/....
server.use('/api', rootRouter);

// * Static Server 
server.use(express.static('public'));

// TODO: mongoose conection

//* Security Config
server.use(helmet());
server.use(cors());

// * Content Type Config
server.use(express.urlencoded({ extended: true, limit: '50mb' }));
server.use(express.json({ limit: '50mb' }));


// * Redirection Config
// * http://localhost:3000/ ->  http://localhost:3000/api/ 
server.get('/', (_req:Request, res: Response) => {
    res.redirect('/api');
});

export default server;